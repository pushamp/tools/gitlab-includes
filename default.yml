image: docker:stable
services:
  - name: docker:dind


stages:
  - build
  - test
  - review
  - staging
  - canary
  - production
  - cleanup

# Test jobs may be disabled by setting environment variables:
# * test: TEST_DISABLED


.base_variables:
  CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
  CI_APPLICATION_TAG: $CI_COMMIT_SHA
  DOCKER_DRIVER: overlay2
  KUBECONFIG: /etc/deploy/config


test:
  stage: test
  script:
    - echo "Running tests"


.base_functions: &base_functions |
  echo $CI_COMMIT_BRANCH 
  echo $CI_DEFAULT_BRANCH
  function ensure_namespace() {
      kubectl get namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
  }

  function delete_namespace() {
    kubectl delete namespaces ${KUBE_NAMESPACE} --ignore-not-found
  }

  function create_secret() {
      echo "Create secret..."
      echo "--docker-server=$CI_REGISTRY --docker-username=$REGISTRY_USER --docker-password=$REGISTRY_PASSWORD"
      kubectl create secret -n "$KUBE_NAMESPACE" \
        docker-registry "gitlab-registry-${CI_PROJECT_PATH_SLUG}" \
        --docker-server="$CI_REGISTRY" \
        --docker-username="${CI_DEPLOY_USER:-$CI_REGISTRY_USER}" \
        --docker-password="${CI_DEPLOY_PASSWORD:-$CI_REGISTRY_PASSWORD}" \
        --docker-email="$GITLAB_USER_EMAIL" \
        -o yaml --dry-run | kubectl replace -n "$KUBE_NAMESPACE" --force -f -
  }

  function deploy() {
    echo "Comming soon..."
  }

  function build() {
      if [[ -n "$CI_REGISTRY_USER" ]]; then
        echo "Logging to GitLab Container Registry with CI credentials..."
        docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
        echo ""
      fi

      if [[ -f Dockerfile ]]; then
        echo "Building Dockerfile-based application..."
      fi

      image_previous="$CI_APPLICATION_REPOSITORY:$CI_COMMIT_BEFORE_SHA"
      image_tagged="$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"
      image_latest="$CI_APPLICATION_REPOSITORY:latest"
      echo $image_latest
      # pull images for cache - this is required, otherwise --cache-from will not work
      docker image pull --quiet "$image_previous" || \
        docker image pull --quiet "$image_latest" || \
        true

      docker build \
        --cache-from "$image_previous" \
        --cache-from "$image_latest" \
        --tag "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG" \
        --tag "$image_latest" .

      echo "Pushing to GitLab Container Registry..."
      docker push "$image_tagged"
      docker push "$image_latest"
      echo ""
    }
  
    function deploy() {
      diver --version
      export track="${1-stable}"
      if [[ -z "$CI_COMMIT_TAG" ]]; then
        image_repository=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
        image_tag=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}
      else
        image_repository=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
        image_tag=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG}
      fi
      export DOCKER_IMAGE="$image_repository:$image_tag"
      echo "track: $track"
      echo "CI_APPLICATION_REPOSITORY: $CI_APPLICATION_REPOSITORY"
      echo "INGRESS_HOSTS: $INGRESS_HOSTS"
      echo "CI_ENVIRONMENT_SLUG: $CI_ENVIRONMENT_SLUG"
      echo "CI_PROJECT_PATH_SLUG: $CI_PROJECT_PATH_SLUG"
      echo "DOCKER_IMAGE: $DOCKER_IMAGE"
      echo "INGRESS_HOSTS: $INGRESS_HOSTS"
      echo "KUBE_REPL: $KUBE_REPL"
      echo "SERVICE_NAME: $SERVICE_NAME"
      # Deploy normaly (staging or production)
      if [ $track = "stable" ] && ! [ $CI_JOB_STAGE = "review" ]; then
        diver get_k8s --tls_enable \
        --ci_env $CI_ENVIRONMENT_SLUG  \
        --ci_app $CI_PROJECT_PATH_SLUG \
        --image $DOCKER_IMAGE \
        --ingress_hosts $INGRESS_HOSTS \
        --replicas $KUBE_REPL \
        --service_name "$CI_ENVIRONMENT_SLUG" \
        --tasks_replicas $TASK_REPLICAS > deploy.yaml;
      # Deploy if canary
      elif [ $track = "canary" ]; then
        diver get_k8s \ 
        --tls_enable \ 
        --no_tasks --is_canary \
        --ci_env $CI_ENVIRONMENT_SLUG  \
        --ci_app $CI_PROJECT_PATH_SLUG \
        --image $DOCKER_IMAGE \
        --ingress_hosts $INGRESS_HOSTS \
        --replicas $KUBE_REPL \
        --service_name "$CI_ENVIRONMENT_SLUG" > deploy.yaml;
      # Deploy if review
      else
       echo "deploy review"
       rev_ing_host=$(diver get_ingresses --hostname ${CI_PROJECT_ID}-${CI_ENVIRONMENT_SLUG}.${KUBE_INGRESS_BASE_DOMAIN})
       echo $rev_ing_host
       echo $CI_ENVIRONMENT_SLUG
       echo $CI_PROJECT_PATH_SLUG
       echo $DOCKER_IMAGE
       echo $KUBE_REPL
       echo "$CI_ENVIRONMENT_SLUG"
       echo "Start make manifest"
       diver get_k8s --no_tasks --tls_enable --ci_env $CI_ENVIRONMENT_SLUG --ci_app $CI_PROJECT_PATH_SLUG --image $DOCKER_IMAGE --replicas $KUBE_REPL --ingress_hosts ${rev_ing_host} --service_name "$CI_ENVIRONMENT_SLUG" > deploy.yaml;
      fi
      echo "Manifest maked"
      cat deploy.yaml
      kubectl --namespace ${KUBE_NAMESPACE} apply --dry-run -f deploy.yaml
      kubectl --namespace ${KUBE_NAMESPACE} apply -f deploy.yaml
  }


before_script:
  - *base_functions

build:
  stage: build
  script:
    - build

.auto-deploy:
  image: registry.gitlab.com/pushamp/tools/docker-kubectl-pushamp:latest
  before_script:
    - mkdir -p /etc/deploy
    - echo ${kube_config} | base64 -d > ${KUBECONFIG}
    - *base_functions

review:
  extends: .auto-deploy
  stage: review
  script:
     - env
     - ensure_namespace
     - create_secret
     - deploy
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: http://$CI_PROJECT_ID-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN${WEB_ROOT_PATH:-/api/doc}
    on_stop: stop_review
    auto_stop_in: 3 day
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: never
    - if: '$REVIEW_DISABLED'
      when: never
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'

stop_review:
  extends: .auto-deploy
  stage: cleanup
  variables:
    GIT_STRATEGY: none
  script:
    - ensure_namespace
    - delete_namespace
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  allow_failure: true
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: never
    - if: '$REVIEW_DISABLED'
      when: never
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
      when: manual

staging:
  extends: .auto-deploy
  stage: staging
  script:
    - echo $CI_COMMIT_BRANCH 
    - echo $CI_DEFAULT_BRANCH
    - echo "Deploy to staging server"
    - ensure_namespace
    - create_secret
    - deploy
    - echo $CI_ENVIRONMENT_URL
    - echo $DYNAMIC_ENVIRONMENT_URL
    - echo "Done"
  environment:
    name: staging
    url: http://$CI_PROJECT_PATH_SLUG-staging.$KUBE_INGRESS_BASE_DOMAIN
  artifacts:
    paths:
    - deploy.yaml
    expire_in: 3 week
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

production:
  extends: .auto-deploy
  stage: production
  script:
    - echo "Deploy to production server"
    - ensure_namespace
    - create_secret
    - echo $CI_ENVIRONMENT_URL
    - echo $DYNAMIC_ENVIRONMENT_URL
    - deploy
    - echo "Done"

  environment:
    name: production
    url:  http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN

  artifacts:
    paths:
    - deploy.yaml
    expire_in: 3 week

  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: manual
  
